# Arcade game "Frogger"


This game is a clone of the classic arcade game Frogger.
Click [here](https://swu2050.bitbucket.io/javascript-arcade-game/) to play.


# How to play


To move the character, you use the keys: up, down, left, right.    
Your goal is to cross the road without getting hit by the bugs.
Whenever you score a point, the enemy bugs will run faster.


# Credit
All the assets are provided by Udacity. This was a project to learn about Object
Oriented Programming in JavaScript. It was the final project for the class.
Click [https://www.udacity.com/course/object-oriented-javascript--ud015](https://www.udacity.com/course/object-oriented-javascript--ud015) for more information