// Enemies our player must avoid
var Enemy = function(x, y, speed) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    // The image/sprite for our enemies, this uses
    // a helper we've provided to easily load images
    this.sprite = 'images/enemy-bug.png';
};

// Update the enemy's position, required method for game
// Parameter: dt, a time delta between ticks
Enemy.prototype.update = function(dt) {
    // You should multiply any movement by the dt parameter
    // which will ensure the game runs at the same speed for
    // all computers.
    this.x = this.x + this.speed * dt;
    //reset enemies to the left when they hit the end of the canvas.
    if (this.x >= window.ctx.canvas.width) {
        this.x = 0;
    }

    checkCollisions(this);
};

// Draw the enemy on the screen, required method for game
Enemy.prototype.render = function() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.
var Player = function(x, y, speed) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.sprite = 'images/char-boy.png';
};

Player.prototype.render = function() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

Player.prototype.update = function() {
  if(this.y == 0){
    this.x = 100;
    this.y = 400;
    score++;
    window.document.getElementById('score_id').innerHTML= score;
    makeEnemiesFaster();
  }
};

Player.prototype.handleInput = function(key) {

    var stepsToMove = 25;

    if (key == 'left') {
        this.x -= stepsToMove;
        // make sure moving doesnt make the player run through the wall.
        if (this.x < 3) {
            this.x = 3
        }
    }

    if (key == 'right') {
        this.x += stepsToMove;
        // make sure moving doesnt make the player run through the wall.
        if (this.x > window.ctx.canvas.width - 100) {
            this.x = window.ctx.canvas.width - 100;
        }
    }

    if (key == 'up') {
        this.y -= stepsToMove;
        // make sure moving doesnt make the player run through the wall.
        if (this.y < 0) {
            this.y = 0;
        }
    }

    if (key == 'down') {
        this.y += stepsToMove;
        if (this.y > window.ctx.canvas.height - 175) {
            this.y = window.ctx.canvas.height - 175;
        }
    }
};

var checkCollisions = function(enemy){
  if(
      player.y + 130 >= enemy.y + 90
      && player.x + 25 <= enemy.x + 90
      && player.y + 70 <= enemy.y + 130
      && player.x + 70 >= enemy.x + 10
    ){
      player.x = 100;
      player.y = 400;
      score--;
      window.document.getElementById('score_id').innerHTML = score;
  }
};

var makeEnemiesFaster = function () {
  allEnemies.forEach(function(element){
    element.speed += 20;
  });
};
// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
// Place the player object in a variable called player
var allEnemies = [];
var player = new Player(100, 400, 50);
var enemy = new Enemy(0, Math.random() * 184 + 50, Math.random() * 300);
var enemy2 = new Enemy(0, Math.random() * 184 + 50, Math.random() * 300);
var enemy3 = new Enemy(0, Math.random() * 184 + 50, Math.random() * 300);
var  score = 0;
allEnemies.push(enemy);
allEnemies.push(enemy2);
allEnemies.push(enemy3);
// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function(e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    };

    player.handleInput(allowedKeys[e.keyCode]);
});
